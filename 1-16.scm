(define (expt b n)
    (define (iter b n a)
      (display (list b n a))
      (newline)
      (cond ((= n 0) a)
            ((even? n) (iter (* b b) (/ n 2) a))
            (else (iter b (- n 1) (* b a)))))
    (iter b n 1))
