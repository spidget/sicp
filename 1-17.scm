(define (double a) (+ a a))

(define (halve a) (/ a 2))

(define (fast-expt b n)
  (cond ((= n 0) 1)
        ((even? n) (square (fast-expt b (/ n 2))))
        (else (* b (fast-expt b (- n 1))))))

(define (* a b)
  (display (list a b))
  (newline)
  (cond ((= b 0) 0)
        ((= b 1) a)
        ((even? b) (double (* a (halve b))))
        (else (+ a (* a (- b 1))))))

(* 2 3)
(+ 3 (* 2 2))
