(define (double a) (+ a a))

(define (halve a) (/ a 2))

(define (* a b)
  (define (iter a b c)
    (display (list a b c))
    (newline)
    (cond ((= b 0) c)
          ((even? b) (iter (double a) (halve b) c))
          (else (iter a (- b 1) (+ a c)))))
  (iter a b 0))
